﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalOrange : MonoBehaviour
{
    public GameObject Point;
    public GameObject Balle;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter2D(Collision2D collision){
        if(collision.gameObject.CompareTag("Ball")){
            Destroy(collision.gameObject);
            var nbPoint = GameObject.FindGameObjectsWithTag("Orange_Point"); 
            var nbTotal = 1;
            foreach(GameObject nb in nbPoint){
                nbTotal = nbTotal+1;
            }
            var newPoint = Instantiate(Point,new Vector3(nbTotal * -2F, 20F, 0), Quaternion.identity);
            if(nbTotal == 5){
                var nbPointsBleu = GameObject.FindGameObjectsWithTag("Blue_Point"); 
                foreach(GameObject point in nbPointsBleu){
                    Destroy(point);
                }
                var nbPointsOrange = GameObject.FindGameObjectsWithTag("Orange_Point"); 
                foreach(GameObject point in nbPointsOrange){
                    Destroy(point);
                }
            }
            StartCoroutine(Wait3Seconds());
        }
        
    }

    IEnumerator Wait3Seconds()
    {
        yield return new WaitForSeconds(3);
        var balle = Instantiate(Balle,new Vector3(0, 0, 0), Quaternion.identity);
        var Girl_Left = GameObject.FindGameObjectsWithTag("Girl_Left");
        Girl_Left[0].transform.position = new Vector3(-10, 0.5F, 0);
        var Girl_Right = GameObject.FindGameObjectsWithTag("Girl_Right");
        Girl_Right[0].transform.position = new Vector3(10, 0.5F, 0);
    }
}
