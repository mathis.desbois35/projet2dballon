﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public GameObject EffectHit;
    public GameObject EffectGoal;
    public AudioClip GoalSound;
    // private float movementSpeed = 3f;
    // private int nextUpdate = 1;
    // private float randomX;
    // private float randomY;
    
    void Start()
    {
        transform.position = new Vector3(0, 0, 0);
        GetComponent<AudioSource> ().playOnAwake = false;
        GetComponent<AudioSource> ().clip = GoalSound;
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnCollisionEnter2D(Collision2D other){
        if(other.gameObject.CompareTag("Girl_Left") || other.gameObject.CompareTag("Girl_Left")){
            Instantiate(EffectHit, (Vector3)other.contacts[0].point , Quaternion.identity);
        }
        if(other.gameObject.CompareTag("Goal_Orange") || other.gameObject.CompareTag("Goal_Blue")){
            Instantiate(EffectGoal, (Vector3)other.contacts[0].point , Quaternion.identity);
            GetComponent<AudioSource>().Play();
        }
    }


}
