﻿using UnityEngine;

 [RequireComponent(typeof(SpriteRenderer), typeof(Animator), typeof(Rigidbody2D))]
 public class AnimateGirlSpritesheet : MonoBehaviour {
    public KeyCode MoveRightKey = KeyCode.RightArrow;
    public KeyCode MoveLeftKey = KeyCode.LeftArrow;
    public KeyCode MoveUpKey = KeyCode.UpArrow;
    public KeyCode MoveDownKey = KeyCode.DownArrow;

    SpritesheetAnimator animator;
    SpriteRenderer spriteRenderer;
    private float movementSpeed = 10f;
    // public GameObject Monprefab;




    void Start() {
        animator = GetComponent<SpritesheetAnimator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void FixedUpdate() {
        Vector3 move = Vector3.zero;
        if(Input.GetKey(MoveRightKey) || Input.GetKey(MoveLeftKey) || Input.GetKey(MoveUpKey) || Input.GetKey(MoveDownKey)){
            if (Input.GetKey(MoveRightKey)) {
                move += Vector3.right * movementSpeed;
                spriteRenderer.flipX = false;
                animator.Play(Anims.Run);
            }
            if (Input.GetKey(MoveLeftKey)) {
                move += Vector3.left * movementSpeed;
                spriteRenderer.flipX = true;
                animator.Play(Anims.Run);
            }
            if (Input.GetKey(MoveUpKey)) {
                move += Vector3.up * movementSpeed;
                animator.Play(Anims.Climb);
            }
            if (Input.GetKey(MoveDownKey)) {
                move += Vector3.down * movementSpeed;
                animator.Play(Anims.Roll);
            }
        }else{
            animator.Play(Anims.Iddle);
        }
        GetComponent<Rigidbody2D>().velocity = move;
    }
}